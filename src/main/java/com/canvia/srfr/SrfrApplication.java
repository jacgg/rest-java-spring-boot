package com.canvia.srfr;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SrfrApplication {

	public static void main(String[] args) {
		SpringApplication.run(SrfrApplication.class, args);
	}

}
